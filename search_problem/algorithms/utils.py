"""
defines search problem and algorithms for finding best assignment
"""
from search_problem.src.CalenderTipes import *
def null_heuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0


def day_value_heuristic(day: EventsList, extractor: CalendarInfoExtractor, print_scores=False):
    """:returns score value for day givent a calender"""
    events = day.get_events()
    some = 0
    for event in events:
        if event.moveable is True:
            # intersecting_percentage will rather options that are simmler to are history
            # hits_count will rather events that we have more information about
            value = extractor.intersecting_percentage(event) * extractor.hits_count(event)
            if print_scores:
                print(event.summary + ":   value=" + str(value))
            some += value
    return some


def day_min_value_hueristic(day: EventsList, extractor: CalendarInfoExtractor):
    """
    terns day heuristic to min function (smaller is better) if day heuristic is changes need to change this function to
    """
    # func =  lambda x: extractor.count_occurrences(x.summary) - extractor.intersecting_percentage(x) * extractor.hits_count(x)
    # some = 0
    # for event in day.get_events():
    #     if event.moveable is True:
    #         hits =extractor.hits_count(event)
    #         if hits ==0:
    #             hits = 1
    #         value = (1 - extractor.intersecting_percentage(event)) / hits
    #         some += value
    # return some
    return -1 * day_value_heuristic(day, extractor)



