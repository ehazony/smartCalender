import copy

from search_problem.src.CalenderTipes import Event, EventsList, Calendar, CalendarInfoExtractor
from search_problem.algorithms.SearchProblem import SearchProblem
from search_problem.algorithms.utils import CalNode, day_min_value_hueristic, day_value_heuristic
from search_problem.search_blokus import a_star_search


class ScheduleSearchProblemAStar(SearchProblem):
    def __init__(self, cal: Calendar, base_day: EventsList = EventsList([]), start=None, end=None, events_data=None):
        self.heuristic = _heuristic
        self.set_name("AStar")
        self.base_day = base_day
        self.events_to_schedule = events_data
        self.cal = cal
        self.extractor = CalendarInfoExtractor(cal)
        self.set_start(start)
        self.set_end(end)
        self.expanded = 0

    def is_goal_state(self, node: CalNode):
        return len(node.events_to_schedule) == 0

    def get_start_state(self):
        return CalNode(self.base_day, self.events_to_schedule)

    def get_assignment(self, base_day, event_data):
        self.set_start(base_day.start)
        self.set_end(base_day.end)
        solution = self.get_Astar_assignment(base_day, event_data)
        return solution, day_value_heuristic(solution, self.extractor), self.expanded

    def get_cost_of_actions(self, events: EventsList):
        """

        :param events: eventsList
        :return:
        """

        return day_min_value_hueristic(events, self.extractor)

    def get_successors(self, node: CalNode):
        """
        state: Search state
        For a given state, this should return a list of triples,
        (successor, stepCost), where 'successor' is a
        successor to the current state, and 'stepCost' is the incremental
        cost of expanding to that successor
        """

        base_value = day_min_value_hueristic(node.state, self.extractor)
        successors = []
        state = node.state
        free_intervals = state.get_free_intervals(self.start, self.end)
        for event_data in node.events_to_schedule:
            event_summary, event_delta = event_data.summary, event_data.duration
            placeable_intervals = []
            [placeable_intervals.extend(self.extractor._brake_interval(interval, event_delta)) for
             interval in free_intervals]
            for inter in placeable_intervals:
                events = copy.copy(state.get_events())
                new_event = Event(event_summary, inter.start, inter.end)
                events.append(new_event)
                assignmet = CalNode(EventsList(events, state.get_start(), state.get_end()),
                                    [d for d in node.events_to_schedule if d != event_data])
                assignmet_value = base_value + self.get_cost_of_actions(EventsList([new_event]))
                successors.append((assignmet, assignmet_value))
        return successors

    # def _heuristic(self, node: CalNode):
    #     score = 0
    #     for data in node.events_to_schedule:
    #         assignment, min_value = self._get_best_assingment_event(node.state, data)
    #         score += min_value
    #     return score

    def _get_best_assingment_event(self, state, event_data):
        """
        :return: returns event in the spot of the best possible free interval
        """
        event_summary, event_delta = event_data.summary, event_data.duration
        placeable_intervals = []
        free_intervals = state.get_free_intervals(self.start, self.end)
        [placeable_intervals.extend(self.extractor._brake_interval(interval, event_delta)) for
         interval in free_intervals]
        min_event = None
        min_value = 1
        min_event = Event(event_summary, placeable_intervals[0].start, placeable_intervals[0].end)
        min_value = self.get_cost_of_actions(EventsList([min_event]))
        for inter in placeable_intervals:
            new_event = Event(event_summary, inter.start, inter.end)
            value = self.get_cost_of_actions(EventsList([new_event]))
            if value < min_value:
                min_event = new_event
                min_value = value
        return min_event, min_value

    def get_Astar_assignment(self, base_day, events_data):
        self.expanded = 0
        self.events_to_schedule = events_data
        self.base_day = base_day
        s = a_star_search(self, self.heuristic)
        return s.state


def _heuristic(node, problem: ScheduleSearchProblemAStar):
    score = 0
    for data in node.events_to_schedule:
        assignment, min_value = problem._get_best_assingment_event(node.state, data)
        score += min_value
    return score
