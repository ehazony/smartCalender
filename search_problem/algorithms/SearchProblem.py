from pytz import timezone

from search_problem.src import util
from search_problem.src.CalenderTipes import EventsList

DEFUALT_TZ = timezone('Asia/Jerusalem')


class SearchProblem(object):
    def __init__(self):
        self.base_day = None
        self.start = None
        self.end = None
        self.name = None

    def set_base_day(self, base_day):
        self.base_day = base_day

    def set_start(self, start):
        self.start = start
        if self.start is not None and self.start.tzinfo == None:
            self.start = self.start.replace(tzinfo=DEFUALT_TZ)

    def set_end(self, end):
        self.end = end
        if self.end is not None and self.end.tzinfo == None:
            self.end = self.end.replace(tzinfo=DEFUALT_TZ)

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    """
    This class outlines the structure of a search_problem problem
    """

    def get_start_state(self):
        """
        Returns the start state for the search_problem problem
        """

        util.raiseNotDefined()

    def is_goal_state(self, state):
        """
        state: Search statex

        Returns True if and only if the state is a valid goal state
        """
        util.raiseNotDefined()

    def get_successors(self, state):
        """
        state: Search state

        For a given state, this should return a list of triples,
        (successor, action, stepCost), where 'successor' is a
        successor to the current state, 'action' is the action
        required to get there, and 'stepCost' is the incremental
        cost of expanding to that successor
        """
        util.raiseNotDefined()

    def get_assignment(self, base_day: EventsList, event_data):
        """
        :returns:  (solution, score, nodes expanded)
        """
        util.raiseNotDefined()
