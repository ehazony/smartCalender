import math

from search_problem.src.CalenderTipes import *
from search_problem.algorithms.SearchProblem import SearchProblem
from search_problem.algorithms.utils import day_value_heuristic


class ScheduleSearchProblemBFS(SearchProblem):
    """
    solution will use BFS to find optimal day_value_heuristic for any given plan.
    """
    expanded = 0
    best_cost = math.inf

    def __init__(self, cal: Calendar, base_day: EventsList = EventsList([]), start=None, end=None, events_data=None):
        """
        :param base_day: EventState holding all _events that are already in the time period, will
        :param events_data: tuple (summary, duration)
        :param cal: calendar
        """
        self.set_name("BFS")
        self.base_day = base_day
        self.events_to_schedule = events_data
        self.cal = cal
        self.extractor = CalendarInfoExtractor(cal)
        self.set_start(start)
        self.set_end(end)
        for event in self.base_day:  # make shure that _events are not moved in the search_problem
            event.set_moveable(False)
            assert event.start_time >= self._start
            assert event.end_time <= self._end

    def get_assignment(self, base_day, event_data):
        self.set_start(base_day.start)
        self.set_end(base_day.end)

        for event in base_day.get_events():
            assert event.moveable is False
        score, solution = self.get_best_assignment_bfs(base_day, event_data)
        return solution, score, self.expanded

    def get_best_assignment_bfs(self, base_day, events_data, print_counter=False):
        """
        will calculate the best assignment of events_to_schedule by checking all passable states
        using day_heuristic to evaluate
        """
        if len(events_data) == 0:
            self.expanded += 1
            if print_counter:
                print(self.expanded)
            return day_value_heuristic(base_day, self.extractor), base_day
        event_summary, event_delta = events_data[0].summary, events_data[0].duration
        placeable_intervals = base_day.get_placement_intervals(event_delta)
        max_value = 0
        max_assignment = None
        for inter in placeable_intervals:
            events = copy.copy(base_day.get_events())
            events.append(Event(event_summary, inter.start, inter.end))
            assignment = EventsList(events, base_day.get_start(), base_day.get_end())
            value, s = self.get_best_assignment_bfs(assignment, events_data[1:], print_counter)
            if value > max_value:
                max_value = value
                max_assignment = s
        return max_value, max_assignment
