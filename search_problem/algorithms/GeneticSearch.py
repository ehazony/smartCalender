from operator import attrgetter

from search_problem.algorithms.SearchProblem import SearchProblem
from search_problem.algorithms.utils import *

population_size = 25
generations = 20
crossover_probability = 0.8
mutation_probability = 0.4
elitism = True


class GASearchProblem(SearchProblem):
    def __init__(self, cal: Calendar, start=None, end=None, events_data=None):
        # GA params

        self.selection_function = self.random_selection
        self.population_size = population_size
        self.generations = generations
        self.crossover_probability = crossover_probability
        self.mutation_probability = mutation_probability
        self.elitism = elitism
        self.current_generation = []
        # search params
        self.set_name("GA")
        self.expanded = 0
        self.base_day = None
        self.events_to_schedule = events_data
        self.cal = cal
        self.extractor = CalendarInfoExtractor(cal)
        self.start = start
        self.end = end
        self.tasks_info = None

    def create_individual(self, base_day: EventsList, tasks_info):
        day = base_day.copy()
        for task in tasks_info:
            intervals = day.get_placement_intervals(task.duration)
            inter = random.choice(intervals)
            day.add_event(Event(task.summary, inter.start, inter.end))
        return day

    def crossover_function(self, plan1: EventsList, plan2: EventsList):
        new_plan= self.base_day.copy()
        movable_events1 = plan1.get_movable_events()
        random.shuffle(movable_events1)
        movable_events2 = plan2.get_movable_events()
        num_events = len(movable_events1)
        new_plan.add_events_list(movable_events1[:num_events // 2])
        for e in movable_events1[num_events // 2:]:
            for e1 in movable_events2:
                if e.same_task(e1):
                    if new_plan.canAdd(e1):
                        new_plan.add_event(e1)
                    else:
                        new_plan.add_event(e)
        assert len(new_plan.get_movable_events()) == num_events
        return new_plan

    def mutate_function(self, day: EventsList):
        """
        pics random event and randomly selects a new place for it
        """
        event = random.choice(day.get_movable_events())
        summery, delta = event.summary, event.end_time - event.start_time
        day.get_events().remove(event)
        day.add_random_event(task(summery, duration=delta))

    def random_selection(self, population):
        """Select and return a random member of the population."""
        return random.choice(population)

    def tournament_selection(self, population):
        """Select a random number of individuals from the population and
        return the fittest member of them all.
        """
        if self.tournament_size == 0:
            self.tournament_size = 2
        members = random.sample(population, self.tournament_size)
        members.sort(
            key=attrgetter('fitness'), reverse=True)
        return members[0]

    def create_initial_population(self):
        """Create members of the first population randomly.
        """
        initial_population = []
        for _ in range(self.population_size):
            genes = self.create_individual(self.base_day, tasks_info=self.tasks_info)
            individual = Chromosome(genes)
            initial_population.append(individual)
        self.current_generation = initial_population

    def create_first_generation(self):
        self.create_initial_population()
        self.calculate_population_fitness()
        self.rank_population()

    def calculate_population_fitness(self):
        """Calculate the fitness of every member of the given population using
        the supplied fitness_function.
        """
        for individual in self.current_generation:
            self.expanded += 1
            individual.fitness = day_value_heuristic(individual.genes, self.extractor)

    def rank_population(self):
        """Sort the population by fitness according to the order defined by
        maximise_fitness.
        """
        self.current_generation.sort(
            key=attrgetter('fitness'), reverse=True)

    def create_new_population(self):
        """Create a new population using the genetic operators (selection,
        crossover, and mutation) supplied.
        """
        new_population = []
        elite = self.current_generation[0].copy()
        selection = self.selection_function

        while len(new_population) < self.population_size:
            parent_1 = selection(self.current_generation)
            parent_2 = selection(self.current_generation)

            child_1, child_2 = parent_1, parent_2
            child_1.fitness, child_2.fitness = 0, 0

            can_crossover = random.random() < self.crossover_probability
            can_mutate = random.random() < self.mutation_probability

            if can_crossover:
                child_1.genes = self.crossover_function(parent_1.genes, parent_2.genes)
                child_2.genes = self.crossover_function(parent_2.genes, parent_1.genes)

            if can_mutate:
                self.mutate_function(child_1.genes)
                self.mutate_function(child_2.genes)

            new_population.append(child_1)
            if len(new_population) < self.population_size:
                new_population.append(child_2)

        if self.elitism:
            new_population[0] = elite

        self.current_generation = new_population

    def create_first_generation(self):
        """Create the first population, calculate the population's fitness and
        rank the population by fitness according to the order specified.
        """
        self.create_initial_population()
        self.calculate_population_fitness()
        self.rank_population()

    def create_next_generation(self):
        """Create subsequent populations, calculate the population fitness and
        rank the population by fitness in the order specified.
        """
        self.create_new_population()
        self.calculate_population_fitness()
        self.rank_population()

    def best_individual(self):
        """Return the individual with the best fitness in the current
        generation.
        """
        best = self.current_generation[0]
        return best

    def last_generation(self):
        """Return members of the last generation as a generator function."""
        return ((member.fitness, member.genes) for member
                in self.current_generation)

    def get_ga_assignment(self):
        self.create_first_generation()
        for _ in range(1, self.generations):
            self.create_next_generation()
        return self.best_individual().genes

    def get_assignment(self, base_day: EventsList, event_data):
        self.set_start(base_day.start)
        self.set_end(base_day.end)
        self.base_day = base_day
        self.tasks_info = event_data
        self.expanded = 0
        solution = self.get_ga_assignment()
        return solution, day_value_heuristic(solution, self.extractor), self.expanded


class Chromosome(object):
    """ Chromosome class that encapsulates an individual's fitness and solution
    representation.
    """
    def copy(self):
        c= Chromosome(self.genes.copy())
        c.fitness = self.fitness
        return c
    def __init__(self, genes):
        """Initialise the Chromosome."""
        self.genes = genes
        self.fitness = 0

    def __repr__(self):
        """Return initialised Chromosome representation in human readable form.
        """
        return repr((self.fitness, self.genes))
