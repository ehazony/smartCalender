import math

from search_problem.src.CalenderTipes import *
from search_problem.algorithms.SearchProblem import SearchProblem
from search_problem.algorithms.utils import  CalNode, day_min_value_hueristic



class ScheduleSearchProblemTrim(SearchProblem):
    expanded = 0
    best_cost = math.inf

    def __init__(self, cal: Calendar, base_day: EventsList = EventsList([]), start=None, end=None, events_data=None):
        """
        :param base_day: EventState holding all _events that are already in the time period, will
        :param events_data: tuple (summary, duration)
        :param cal: calendar
        """
        self.set_name("TRIM")
        self.base_start_time = base_day.start
        self.base_end_time = base_day.end
        self.base_day = base_day
        self.events_to_schedule = events_data
        self.cal = cal
        self.extractor = CalendarInfoExtractor(cal)
        self.start = start
        self.end = end
        for event in self.base_day:  # make shure that _events are not moved in the search_problem
            event.set_moveable(False)
            assert event.start_time >= self.start
            assert event.end_time <= self.end

    # def get_start_state(self):  # Todo test_info the function on a real day
    #     """adds the _events randamly in the day and return a new state"""
    #     state = EventsList(copy.deepcopy(self.base_day._events), self.base_start_time,
    #                         self.base_end_time)  # copy of the initial _events
    #     state.schedule_random_events(self.events_to_schedule, True)  # add new _events randomly
    #     return state

    def get_successors(self, node: CalNode):
        """
        :param state: base EventsList
        :param events_data: EventsData
        :return: all options to assine task to base state
        """
        successors = []
        state = node.state
        free_intervals = state.get_free_intervals(self.start, self.end)
        for event_data in node.events_to_schedule:
            event_summary, event_delta = event_data.summary, event_data.duration
            placeable_intervals = []
            [placeable_intervals.extend(self.extractor._brake_interval(interval, event_delta)) for
             interval in free_intervals]
            for inter in placeable_intervals:
                events = copy.copy(state.get_events())
                events.append(Event(event_summary, inter.start, inter.end))
                assignmet = CalNode(EventsList(events, state.get_start(), state.get_end()),
                                    [d for d in node.events_to_schedule if d != event_data])
                successors.append(assignmet)
        return successors

    # def get_successors(self, state: EventsList):
    #     """
    #     :returns list of states wich have s single shift of 15 min of one of the _events (movable _events)
    #     :param state: EventsList
    #     :return: list of EventStates
    #     """
    #     shifted = state.get_shiffted_up_eventsStates()
    #     shifted.extend(state.get_shiffted_down_eventsStates())
    #     return shifted

    def get_cost_of_actions(self, actions):
        return 0

    # def get_best_assignment_bfs(self, base_day, events_data, print_counter=False):
    #     """
    #     will calculate the best assignment of events_to_schedule by checking all passable states
    #     using day_heuristic to evaluate
    #     """
    #     if len(events_data) == 0:
    #         self.expanded += 1
    #         if print_counter:
    #             print(self.expanded)
    #         return day_value_heuristic(base_day, self.extractor), base_day
    #     free_intervals = base_day.get_free_intervals(self._start, self._end)
    #     event_summary, event_delta = events_data[0].summary, events_data[0].duration
    #     placeable_intervals = []
    #     [placeable_intervals.extend(self.extractor._brake_interval(interval, event_delta)) for
    #      interval in free_intervals]
    #     max_value = 0
    #     max_assignment = None
    #     for inter in placeable_intervals:
    #         events = copy.copy(base_day.get_events())
    #         events.append(Event(event_summary, inter.start, inter.end))
    #         assignmet = EventsList(events, base_day.get_start(), base_day.get_end())
    #         value, s = self.get_best_assignment_bfs(assignmet, events_data[1:], print_counter)
    #         # print("value: "+ str(value))
    #         if value > max_value:
    #             max_value = value
    #             max_assignment = s
    #     return max_value, max_assignment

    def get_assignment(self, base_day, event_data):
        self.set_start(start)
        self.set_end(end)
        score, solution = self.get_best_assignment_trim(base_day, event_data, 0)
        return solution, score, self.expanded

    def get_best_assignment_trim(self, base_day, events_data, cur_plan_cost, print_counter=False):
        """
        will calculate the best assignment of events_to_schedule by checking all passable states
        using day_heuristic to evaluate
        """
        if len(events_data) == 0:
            if cur_plan_cost < self.best_cost:
                self.best_cost = cur_plan_cost
            return cur_plan_cost, base_day
        free_intervals = base_day.get_free_intervals(self.start, self.end)
        event_summary, event_delta = events_data[0].summary, events_data[0].duration
        placeable_intervals = []
        [placeable_intervals.extend(self.extractor._brake_interval(interval, event_delta)) for
         interval in free_intervals]
        max_value = math.inf
        max_assignment = None
        for inter in placeable_intervals:
            self.expanded += 1
            events = copy.copy(base_day.get_events())
            event = Event(event_summary, inter.start, inter.end)
            events.append(event)
            assignmet = EventsList(events, base_day.get_start(), base_day.get_end())
            new_cost = cur_plan_cost + day_min_value_hueristic(EventsList([event]), self.extractor)
            if new_cost > self.best_cost:
                value = math.inf
                s = math.inf
            else:
                value, s = self.get_best_assignment_trim(assignmet, events_data[1:], new_cost, print_counter)
            # print("value: "+ str(value))
            if value < max_value:
                max_value = value
                max_assignment = s
        return max_value, max_assignment
