import copy

from simanneal import Annealer

from search_problem.algorithms.SearchProblem import SearchProblem
from search_problem.algorithms.utils import day_min_value_hueristic, CalendarInfoExtractor, Event, \
    day_value_heuristic
from search_problem.src.CalenderTipes import EventsList, task, Calendar
import time
import random


class AnnealHelper(Annealer):
    def __init__(self, state, extracter):
        self.extracter = extracter
        super(AnnealHelper, self).__init__(state)

    def move(self):
        day = self.state
        event = random.choice(day.get_movable_events())
        summery, delta = event.summary, event.end_time - event.start_time
        day.get_events().remove(event)
        day.add_random_event(task(summery, duration=delta))

    def energy(self):
        return day_min_value_hueristic(self.state, self.extracter)

    start = 0

    def copy_state(self, state: EventsList):
        return state.copy()

    def default_update(self, step, T, E, acceptance, improvement):
        return
        if step == 0:
            self.start = time.time()
            acceptance, improvement = 0, 0
            print("{:<20} {:<20} {:<20} {:<20} {:<20}, {:<20}".format("Temperature", "Energy", "Accept", "Improve",
                                                                      "Elapsed", "Remaining"))
            print("{:<20} {:<20} {:<20} {:<20}, {:<20}, {:<20}".format(T, E, acceptance, improvement, 0, 0))
        else:
            elapsed = time.time() - self.start
            remain = (self.steps - step) * (elapsed / step)
            print("{:<20} {:<20} {:<20} {:<20}, {:<20}, {:<20}".format(T, E, acceptance, improvement, elapsed, remain))


class AnnealProblem(SearchProblem):
    def __init__(self, cal: Calendar, start=None, end=None, events_data=None):
        # search params
        self.helper = None
        self.set_name("ANNEAL")
        self.expanded = 0
        self.events_to_schedule = events_data
        self.cal = cal
        self.extractor = CalendarInfoExtractor(cal)
        self.start = start
        self.end = end
        self.Tmax = 25000.0
        self.Tmin = 2.5
        self.steps = 500
        self.updates = 100

    def create_individual(self, base_day: EventsList, tasks_info):
        day = base_day.copy()
        for task in tasks_info:
            intervals = day.get_placement_intervals(task.duration)
            inter = random.choice(intervals)
            day.add_event(Event(task.summary, inter.start, inter.end))
        return day
    def set_schudle(self, tmax ,tmin, steps, updates):
        self.Tmax = tmax
        self.Tmin = tmin
        self.steps = steps
        self.updates = updates
    def get_assignment(self, base_day: EventsList, event_data):
        self.set_start(base_day.start)
        self.set_end(base_day.end)
        self.tasks_info = event_data
        self.base_day = base_day
        self.expanded = 0
        initial_state = self.create_individual(base_day, tasks_info=event_data)
        self.helper = AnnealHelper(initial_state, self.extractor)
        self.helper.set_schedule({'tmax': self.Tmax, 'tmin': self.Tmin, 'steps': self.steps, 'updates': self.updates})
        # self.helper.copy_strategy= 'method'
        solution, score = self.helper.anneal()
        return solution, day_value_heuristic(solution, self.extractor), self.helper.steps
