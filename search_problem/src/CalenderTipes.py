"""
defines calender objects i.e. event, calendar, calendar_extracter and EventsList.
"""

import datetime
import pickle
from abc import ABC, abstractclassmethod
import random
import pytz
import copy
from search_problem.src.datetime_interval import Interval
from search_problem.src.Google_Cal_api import Google_Cal_api
import pytz.reference
from enum import Enum
import heapq
from pytz import timezone
from search_problem.search_blokus import Node

local_tnz = pytz.reference.LocalTimezone()

SUMMERY = 0
DURATION = 1


class ProblemName(Enum):
    TODAY = 1
    TOMORROW = 2
    THIS_WEEK = 3
    NEXT_WEEK = 4
    ALL = 5


def parce(event_description):
    data = str.split(",")
    return data[SUMMERY], datetime.timedelta(minutes=int(data[DURATION]))


class task(ABC):
    def __init__(self, summary, duration):
        """
        :param summary: string
        :param duration: timeDelta
        """
        self.summary = summary
        self.duration = duration


class CalNode(Node):
    """
    node state for search algorithms
    """

    def __init__(self, base_day, data):
        self.state = base_day
        self.events_to_schedule = data


class Event(ABC):
    def __init__(self, summary, start_time, end_time, moveable=True):
        self.moveable = moveable
        self.start_time = start_time
        self.end_time = end_time
        self.summary = summary
        if self.start_time is not None and self.end_time is not None:
            self.interval = Interval(self.start_time, self.end_time)
        else:
            self.interval = None

    def __eq__(self, other):
        return self.start_time == other.start_time and self.end_time == other.end_time

    def __ne__(self, other):
        return not self.__eq__(other)

    def __lt__(self, other):
        if self.start_time < other.start_time:
            return True
        if self.start_time == other.start_time and self.end_time < other.end_time:
            return True
        return False

    def __str__(self):
        return 'Event summary: %s, _start time %s, _end time %s' % (self.summary, self.start_time, self.end_time)

    def __repr__(self):
        return str(self)

    def same_task(self, other):
        return self.interval.to_timedelta() == other.interval.to_timedelta() and self.summary == other.summary

    @classmethod
    def create_random_event(self, data, start, end):  # todo test_info
        """
        :param summary for event
        :param duration of event
        :param start: floor for _start datetime object
        :param end: sealing for _end datetime object
        :return: randem event that is in the time range, _start time is in qurders of the hour
        """
        duration = data.duration
        summary = data.summary
        assert (duration >= datetime.timedelta(minutes=15))
        assert ((duration.total_seconds() % 3600) // 60 % 15 == 0)
        assert (end - start >= duration)
        delta = end - start
        seconds = (delta - duration).total_seconds()
        minutes, seconds = divmod(seconds, 60)
        i = int(minutes / 15)
        date_list = [start + datetime.timedelta(minutes=15 * x) for x in range(0, i + 1)]
        start_time = random.choice(date_list)
        end_time = start_time + duration
        assert (start <= start_time and start_time <= end_time and end_time <= end)
        return Event(summary, start_time, end_time)

    def summary(self):
        return self.summary

    def start_time(self):
        return self.start_time

    def end_time(self):
        return self.end_time

    def set_start_time(self, start_time):
        self.start_time = start_time
        assert self.end_time >= self.start_time

    def set_end_time(self, end_time):
        self.end_time = end_time
        assert self.end_time >= self.start_time

    def set_moveable(self, bul):
        self.moveable = bul


class GoogleEvent(Event):
    """"Event object with google event as input_data"""
    # TODO fix api to be constent with Event
    api = Google_Cal_api()

    def __init__(self, google_event_data, moveable=False):
        self.event_data = google_event_data
        super().__init__(self.summary(), self.start_time(), self.end_time(), moveable)

    def summary(self):
        try:
            return self.event_data['summary']
        except:
            return None

    def start_time(self):
        try:
            return self.api.isotime_to_datetime(self.event_data['start']['dateTime']).replace(second=0, microsecond=0,
                                                                                              tzinfo=pytz.timezone(
                                                                                                  'Asia/Jerusalem'))
        except:
            return None

    def end_time(self):
        try:
            # print(self.event_data['end'])
            # print(self.event_data['end']['dateTime'])
            # print(self.event_data['end']['dateTime']).replace(second=0, microsecond=0,
            #                                                                                 tzinfo=pytz.timezone(
            #                                                                                     'Asia/Jerusalem'))

            return self.api.isotime_to_datetime(self.event_data['end']['dateTime']).replace(second=0, microsecond=0,
                                                                                            tzinfo=pytz.timezone(
                                                                                                'Asia/Jerusalem'))
        except:
            return None

    def location(self):
        try:
            return self.event_data['location']
        except:
            return ""


class Calendar(ABC):
    @abstractclassmethod
    def get_tomorrow_events(self):
        pass

    @abstractclassmethod
    def get_today_events(self):
        pass

    @abstractclassmethod
    def get_week_events(self):
        pass

    @abstractclassmethod
    def get_nextWeek_events(self):
        pass

    @abstractclassmethod
    def get_all_events(self):
        pass

    def get_events(self, when: ProblemName):
        if ProblemName.TODAY == when:
            return self.get_today_events()
        if ProblemName.TOMORROW == when:
            return self.get_tomorrow_events()
        if ProblemName.THIS_WEEK == when:
            return self.get_week_events()
        if ProblemName.NEXT_WEEK == when:
            return self.get_nextWeek_events()
        if ProblemName.ALL == when:
            return self.get_all_events()


class EventsList:
    """an object for holding a group of _events"""

    def __init__(self, events: [Event], start=None, end=None):
        """
        :param events: a list of _events to be stored ( with start, end, summary) not none
        :param start: datetime object, default is 6 am today
        :param end: datetime object, default is 18 pm today
        """
        self.start = start
        self.end = end
        self.events = []
        self.timezone = timezone('Asia/Jerusalem')
        if start is None and end is None:  # set start and end to today
            self.start = datetime.datetime.now(self.timezone)
            self.start = self.start.replace(hour=6, minute=0, second=0, microsecond=0)
            # self.start.tzinfo = local_tnz.utcoffset(self.start)
            self.end = datetime.datetime.now(self.timezone)
            self.end = self.end.replace(hour=18, minute=0, second=0, microsecond=0)

            # if _events in self._events not in default time (start,end) fix start and end time
            for event in events:
                event.start_time = event.start_time.replace(tzinfo=self.timezone)
                event.end_time = event.end_time.replace(tzinfo=self.timezone)
                if event.start_time < self.start:
                    self.start = event.start_time
                if event.end_time > self.end:
                    self.end = event.end_time

        elif start is None or end is None:
            raise Exception

        # self.end.tzinfo = local_tnz.utcoffset(self.end)
        # self.timezone = local_tnz.utcoffset(self._start)
        self.set_start(tzinfo=self.timezone)
        self.add_events_list(events)

    def __repr__(self):
        s = str(self.start) + "\n"
        s += "\n".join([str(event) for event in self.get_events()])
        s += str(self.end) + "\n"
        return s

    def __iter__(self):
        return iter(self.events)

    def copy(self):
        return EventsList(copy.copy(self.events), start=self.start, end=self.end)

    def get_events(self):
        return self.events

    def set_start(self, year=None, month=None, day=None, hour=None, minute=None,
                  second=None, microsecond=None, tzinfo=None):
        if year is not None:
            self.start = self.start.replace(year=year)
        if month is not None:
            self.start = self.start.replace(month=month)
        if day is not None:
            self.start = self.start.replace(day=day)
        if hour is not None:
            self.start = self.start.replace(hour=hour)
        if minute is not None:
            self.start = self.start.replace(minute=minute)
        if minute is not None:
            self.start = self.start.replace(second=second)
        if minute is not None:
            self.start = self.start.replace(microsecond=microsecond)

        self.start = self.start.replace(tzinfo=tzinfo)
        e = []
        for event in self.events:
            if event.start_time >= self.start:
                e.append(event)
            elif event.end_time > self.start:
                event.start_time = copy.copy(self.start)
                e.append(event)
        self.events = e

    def set_end(self, year=None, month=None, day=None, hour=None, minute=None,
                second=None, microsecond=None, tzinfo=None):

        if year is not None:
            self.end = self.end.replace(year=year)
        if month is not None:
            self.end = self.end.replace(month=month)
        if day is not None:
            self.end = self.end.replace(day=day)
        if hour is not None:
            self.end = self.end.replace(hour=hour)
        if minute is not None:
            self.end = self.end.replace(minute=minute)
        if minute is not None:
            self.end = self.end.replace(second=second)
        if minute is not None:
            self.end = self.end.replace(microsecond=microsecond)

        self.end = self.end.replace(tzinfo=tzinfo)
        e = []
        for event in self.events:
            if event.end_time <= self.end:
                heapq.heappush(e, event)
            elif event.start_time < self.end:
                event.end_time = copy.copy(self.end)
                heapq.heappush(e, event)
        self.events = e

    def get_start(self):
        return self.start

    def get_end(self):
        return self.end

    def add_event(self, event):
        assert event.start_time is not None
        assert event.end_time is not None
        assert event.summary is not None

        # assert event.start_time.tzinfo == event.end_time.tzinfo == self.timezone
        # TODO not shoure we need start and end in eventList
        # if event.start_time < self._start or event.end_time > self._end:
        #     raise Exception
        heapq.heappush(self.events, event)

    def add_events_list(self, events):
        for event in events:
            self.add_event(event)

    def add_random_event(self, data: task, start=None, end=None, moveable=True):
        """
        creats randem event and adds it to its _events
        :param summary for event
        :param timedelta  with the duration of event
        :param start: datetime object representing floor for _start
        :param end: datetime object representing seling for _end
        :param moveable can the event be moved by the app
        """
        if not start:
            start = self.start
        if not end:
            end = self.end
        assert start <= end
        assert end - start >= data.duration
        inter = random.choice(self.get_placement_intervals(data.duration))
        self.add_event(Event(data.summary, inter.start, inter.end))

    def get_placement_intervals(self, event_delta):
        """
        :param event_delta: size of event
        :return: all options for placing event
        """
        assert self.start is not None
        assert self.end is not None
        placeable_intervals = []
        [placeable_intervals.extend(self._brake_interval(interval, event_delta)) for
         interval in self.get_free_intervals(self.start, self.end)]
        return placeable_intervals

    def _brake_interval(self, interval: Interval, delta: datetime.timedelta):
        """
        splits the Interval into delta sized Intervals (15 m differince)
        """
        intervals = []
        fifteen_delta = datetime.timedelta(minutes=15)
        if interval.to_timedelta() < delta:
            return []
        i = interval.start
        while i <= interval.end - delta:
            intervals.append(Interval(i, delta))
            i = i + fifteen_delta
        return intervals

    def get_movable_events(self):
        return [event for event in self.get_events() if event.moveable]

    def get_free_intervals(self, start, end):
        """
        :returns  a list of Intervels, were each interval represents a time in the  Eventstate that dose not have
        a event scheduled on it.
        :return: [Interval]
        """
        # TODO will want this to guest return free_intervals witch will be calculated in the constructor
        if len(self.events) == 0:
            return [Interval(start, end)]

        self.events.sort()
        intervals = []
        if start < self.events[0].start_time:
            intervals.append(Interval(start, self.events[0].start_time))
        for i in range(len(self.events) - 1):
            if self.events[i].end_time < self.events[i + 1].start_time:
                intervals.append(Interval(self.events[i].end_time, self.events[i + 1].start_time))
        if self.end > self.events[-1].end_time:
            intervals.append(Interval(self.events[-1].end_time, end))
        return intervals

    def get_shiffted_up_eventsStates(self, duration=datetime.timedelta(minutes=15)):
        """
        :returns a list of EventStates that each one has a shifted up event in it
        """
        # Todo deal with adjacent _events
        # TODO not completed  function
        sheffted_events = []
        free_intervals = self.get_free_intervals()
        for i in range(len(self.events)):
            events = copy.deepcopy(self.events)
            for interval in free_intervals:
                if events[i].end_time in interval and events[i].end_time + duration in interval:
                    events[i].start_time = events[i].start_time + duration
                    events[i].end_time = events[i].end_time + duration
                    sheffted_events.append(EventsList(events, self.start, self.end))
                    break
        return sheffted_events

    def get_shiffted_down_eventsStates(self, duration=datetime.timedelta(minutes=15)):
        """
        :returns a list of eventsSattes that each one hase a shiffted down event in it
        """
        # Todo deal with adjacent _events
        # TODO not completed  function
        sheffted_events = []
        free_intervals = self.get_free_intervals()
        for i in range(len(self.events)):
            events = copy.deepcopy(self.events)
            for interval in free_intervals:
                if events[i].start_time in interval and events[i].start_time - duration in interval:
                    events[i].start_time = events[i].start_time - duration
                    events[i].end_time = events[i].end_time - duration
                    sheffted_events.append(EventsList(events, self.start, self.end))
                    break
        return sheffted_events

    def schedule_random_events(self, events_data, moveable=True):
        """
        ads new _events in random times in to the EventsList
        :param events_data: [(summary, duration)]      summary is string, duration is timedelta
        """
        for event_data in events_data:
            free_intervals_copy = self.get_free_intervals()
            schedule = False
            while len(free_intervals_copy) > 0:
                i = random.choice(range(len(free_intervals_copy)))
                if event_data[1] > free_intervals_copy[i].duration:
                    free_intervals_copy.pop(i)
                else:
                    self.add_random_event(event_data[0], event_data[1], free_intervals_copy[i].start,
                                          free_intervals_copy[i].end, moveable)
                    schedule = True
                    break
            if not schedule:  # no interval found to put the event
                return None
        return True

    def canAdd(self, event: Event):
        for e in self.events:
            if e.interval.intersection_delta(event.interval) > datetime.timedelta(0):
                return False
        return True


class GoogleCalendar(Calendar):
    def __init__(self):
        self.google_cal_api = Google_Cal_api()
        self.service = self.google_cal_api.service
        self.timezone = pytz.timezone('Asia/Jerusalem')

    def get_events_in_interval(self, start, end):
        assert start < end
        start = start.replace(tzinfo=self.timezone)
        end = end.replace(tzinfo=self.timezone)
        base_plan = EventsList(full_events(self.dict_to_GoogleEvents(self.google_cal_api.get_events(start, end))))
        base_plan.start = start
        base_plan.end = end
        return base_plan

    def get_tomorrow_events(self):
        today = datetime.datetime.now(self.timezone).replace(hour=8, minute=0, second=0, tzinfo=self.timezone)
        tomorrow = today + datetime.timedelta(days=1)
        next_day = (tomorrow + datetime.timedelta(days=1)).replace(hour=0)
        base_plan = EventsList(full_events(self.dict_to_GoogleEvents(self.google_cal_api.get_events(tomorrow,
                                                                                                    next_day))))
        base_plan.start = tomorrow
        base_plan.end = next_day
        return base_plan

    def get_today_events(self):
        today = datetime.datetime.now(self.timezone).replace(hour=8, minute=0, second=0)
        tomorrow = (today + datetime.timedelta(days=1)).replace(hour=0)
        base_plan = EventsList(full_events(
            self.dict_to_GoogleEvents(self.google_cal_api.get_events(today, tomorrow))))
        base_plan.start = today
        base_plan.end = tomorrow
        return base_plan

    def get_week_events(self):
        today = datetime.datetime.now(self.timezone).replace(hour=8, minute=0, second=0, microsecond=0)
        idx = (today.weekday() + 1) % 7
        sun = today - datetime.timedelta(idx)
        saturday = sun + datetime.timedelta(days=7)
        base = EventsList(full_events(
            self.dict_to_GoogleEvents(self.google_cal_api.get_events(sun, sun + datetime.timedelta(days=7)))), sun,
            saturday.replace(hour=0))
        base.start = today
        base.end = saturday
        return base

    def get_nextWeek_events(self):
        today = datetime.datetime.now(self.timezone).replace(hour=0, minute=0, second=0)
        idx = (today.weekday() + 1) % 7
        sun = today - datetime.timedelta(idx - 7)
        saturday = sun + datetime.timedelta(days=7)
        base = EventsList(full_events(
            self.dict_to_GoogleEvents(self.google_cal_api.get_events(sun, sun + datetime.timedelta(days=7)))), sun,
            saturday.replace(hour=24))
        base.start = sun
        base.end = sun + datetime.timedelta(days=7)
        return base

    def get_all_events(self, months=12):
        """get all _events """
        x = 0
        # today = datetime.datetime.now(self.timezone)
        # _start = today - relativedelta(months=months)
        # return self.dict_to_GoogleEvents(self.google_cal_api.get_events(_start, today))

        eventsObj = self.service.events().list(
            calendarId='primary',
            singleEvents=True,
            orderBy='startTime',
        ).execute()
        event_list = EventsList(full_events(self.dict_to_GoogleEvents(eventsObj)))
        next_token = eventsObj.get('nextPageToken')
        while next_token is not None:
            x += 1
            eventsObj = self.service.events().list(
                calendarId='primary',
                singleEvents=True,
                pageToken=next_token,
                orderBy='startTime',
                maxResults=250
            ).execute()
            event_list.add_events_list(full_events(self.dict_to_GoogleEvents(eventsObj)))
            next_token = eventsObj.get('nextPageToken')
        return event_list

        # return EventsList(self.dict_to_GoogleEvents(

    def dict_to_GoogleEvents(self, events):
        e = []
        for v in events.get('items', []):
            e.append(GoogleEvent(v, moveable=False))
        return e

    def _event_to_json(self, event: Event):
        """event format to be added to google calender"""
        # timeZone = pytz.timezone("UTC")
        # timeZone.utcoffset(2)
        # event.start_time=event.start_time.replace(tzinfo=timeZone)
        # event.end_time=event.end_time.replace(tzinfo=timeZone)
        # TODO this is a paster to work only in are timezone needs to be changed!
        start = event.start_time + datetime.timedelta(minutes=21)
        end = event.end_time + datetime.timedelta(minutes=21)
        EVENT = {
            'summary': event.summary,
            'start': {'dateTime': start.isoformat()},
            'end': {'dateTime': end.isoformat()},
        }
        return EVENT

    def insert_event(self, event: GoogleEvent):
        self.google_cal_api.insert_event(self._event_to_json(event))

    def insert_events(self, events: [GoogleEvent]):
        for event in events:
            self.insert_event(event)


class CalendarInfoExtractor:
    def __init__(self, cal: Calendar):
        self.cal = cal
        try:
            self.occurrences_intervals = pickle.load(open("google_calender/occurrences_intervals.pickle", "rb"))
        except (OSError, IOError) as e:
            self.events = cal.get_all_events()
            self.occurrences_intervals = self.get_accurences_intervals(self.events)
            pickle.dump(self.occurrences_intervals, open("google_calender/occurrences_intervals.pickle", "wb"))

        # for x in range(1000):
        #     print(self.intersecting_percentage(self._events._events[x]))

    def get_event_summerys(self):
        return list(self.occurrences_intervals.keys())

    def get_occurrences_intervals(self):
        return self.occurrences_intervals

    def occurrences_in_calendar(self, min_occurences=3):
        summarys = []
        for summary, intervlas in self.occurrences_intervals.items():
            if len(intervlas) >= min_occurences:
                summarys.append(summary)
        return summarys

    def count_occurrences(self, summary):
        """
        :returns how many occurrences of the event is in occurrences_intervals
        """
        if summary in self.occurrences_intervals.keys():
            return len(self.occurrences_intervals[summary])
        return 0

    def intersecting_percentage(self, event: Event):
        """
        :param event: event with _start and _end time
        :return: will compute the percentage of overlap between older _events with the same name as the event
        i.e. average (sum time overlap/ sum time of event in calender)
        """
        summary = event.summary.strip()
        assert event is not None
        assert summary is not None
        if summary not in self.occurrences_intervals:
            return 0  # TODO should this be the value for event that we havent seen?

        some_total = datetime.timedelta(0)
        intersection_sum = datetime.timedelta(0)

        for interval in self.occurrences_intervals[summary]:
            #
            some_total += interval.to_timedelta()
            intersection_sum += interval.intersection_delta(event.interval)

        return intersection_sum / some_total

    def hits_count(self, event: Event):
        """
        computes the number of times the event hit events
        :return:
        """
        hits_counter = 0
        summary = event.summary.strip()
        if summary not in self.occurrences_intervals:
            return 0  # TODO should this be the value for event that we havent seen?
        for interval in self.occurrences_intervals[summary]:
            if interval.intersection_delta(event.interval) > datetime.timedelta(0):
                hits_counter += 1
        return hits_counter

    def get_accurences_intervals(self, events):
        """
        :returns dict{summery, [intervals]} were intervals are the intervals in witch the event has
        accrued
        """
        d = {}
        for event in events:
            if event is None or event.summary is None:
                continue
            summary = event.summary.strip()
            if summary not in d and event.interval is not None:
                d[summary] = []
            if event.interval is not None:  # has a beginning and _end
                d[summary].append(event.interval)
        return d

    # def intervel_intersection_delta(self, interval: Interval, interval1: Interval):
    #     """
    #     :returns timedelta with the duration of intersection of the time
    #      in the day of the to intervals
    #     """
    #     interval._start.timetuple()
    #     return
    def _brake_interval(self, interval: Interval, delta: datetime.timedelta):
        """
        splits the Interval into delta sized Intervals (15 m differince)
        """
        intervals = []
        fifteen_delta = datetime.timedelta(minutes=15)
        if interval.to_timedelta() < delta:
            return []
        i = interval.start
        while i <= interval.end - delta:
            intervals.append(Interval(i, delta))
            i = i + fifteen_delta
        return intervals

    def get_placement_options(self, free_intervals, event_delta):
        """

        :param free_intervals: intervals that event can go in
        :param event_delta: size of event
        :return: all options for placing event
        """
        placeable_intervals = []
        [placeable_intervals.extend(self._brake_interval(interval, event_delta)) for
         interval in free_intervals]

        return placeable_intervals


def full_events(events):
    """
    :returns only _events that have legal parameters start_time end_time and summary
    """
    # TODO make as static methed
    if events is None: return None
    full = []
    for event in events:
        if not (event.start_time is None or event.end_time is None or event.summary is None):
            full.append(event)
    return full


if __name__ == "__main__":
    cal = GoogleCalendar()
    events = cal.get_all_events()
    print(events)
    # [print(i) for i in events.get_free_intervals()]
    # [print(e) for e in events]
# print("---------------------")
# for eventss in _events.get_shiffted_up_eventsStates():
#     [print(e) for e in eventss]
#     print("----------------------")
#
# [print(e) for e in _events]
