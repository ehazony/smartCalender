from __future__ import print_function

from datetime import datetime, timezone, timedelta
import os
SCOPES = 'https://www.googleapis.com/auth/calendar'
GMT_OFF = '+03:00'
import pickle

import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

SERVICE_ACCUNT_FILE ="smartcal-249519-0e680b552f24.json"
CALANDER_NAMES = ["efraimhazony@gmail.com", "לימודים אפרים"]



class Google_Cal_api():
    def __init__(self):
        # here = os.path.dirname(os.path.abspath(__file__))
        #
        # self.store = file.Storage('storage.json')
        # credentials = service_account.Credentials.from_service_account_file(
        #     here + "\\" + SERVICE_ACCUNT_FILE, scopes=SCOPES)
        # # self.creds = self.store.get()
        # self.creds = credentials
        # # if not self.creds or self.creds.invalid:
        # #     flow = client.flow_from_clientsecrets('client_secret.json', SCOPES)
        # #     self.creds = tools.run_flow(flow, self.store)
        # self.service = discovery.build('calendar', 'v3', http=self.creds.authorize(Http()))

        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('google_calender/token.pickle'):
            with open('google_calender/token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'google_calender/client_secret.json', SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('google_calender/token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        self.service = build('calendar', 'v3', credentials=creds)

        # Call the Calendar API
        # now = datetime.datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time
        # print('Getting the upcoming 10 events')
        # events_result = self.service.events().list(calendarId='primary', timeMin=now,
        #                                       maxResults=10, singleEvents=True,
        #                                       orderBy='startTime').execute()
        # events = events_result.get('items', [])

        self.calendar_ids = self.get_calendars_ids()

    def get_calendars_ids(self):
        ids = []
        calendarList = self.service.calendarList().list().execute()
        for l in calendarList["items"]:
            if l["summary"] in CALANDER_NAMES:
                # print(l["summary"])
                ids.append(l["id"])
        return ids

    def create_event(self, summary, start, end):
        """event format to be added to google calender"""
        GMT_OFF = '+03:00'
        EVENT = {
            'summary': summary,
            'start': {'dateTime': '%s%s' % (start, GMT_OFF)},
            'end': {'dateTime': '%s%s' % (end, GMT_OFF)},
        }
        return EVENT

    def dateTime_format(self, year, month, date, time):
        return '%s-%s-%sT%s:00' % (year, month, date, time)

    def print_event(self, e):
        print('''*** %r event added:
            Start: %s
            End:   %s''' % (e['summary'].encode('utf-8'),
                            e['start']['dateTime'], e['end']['dateTime']))

    def insert_event(self, event):

        return self.service.events().insert(calendarId='primary',
                                            sendNotifications=True, body=event).execute()

    def get_events(self, timeMin, timeMax):
        """
        start, finish- datetime objects
        :return google_events
        """
        calendars = []

        for id in self.calendar_ids:
            calendars.append(self.service.events().list(
                calendarId=id,
                timeMax=timeMax.isoformat(),
                timeMin=timeMin.isoformat(),
                singleEvents=True,
                orderBy='startTime',
            ).execute())

        # connectiong calendars
        events = calendars[0]
        for c in calendars[1:]:
            events["items"].extend(c["items"])

        return calendars[0]

    def insert_events(self, events):
        for event in events:
            self.insert_event(event)

    def isotime_to_datetime(self, time):
        """translates  RFC 3339 format to datetimeobject"""
        return datetime.datetime.strptime(''.join(time.rsplit(':', 1)), '%Y-%m-%dT%H:%M:%S%z')


if __name__ == "__main__":
    g = Google_Cal_api()
    # e = g.create_event("testing google api", g.dateTime_format("2018", "09", "28", "19:00"),g.dateTime_format("2018", "08", "28", "20:00"))
    # g.insert_event(e)

    page_token = None
    while True:
        local_time = datetime.now(timezone.utc).astimezone()
        events = g.service.events().list(calendarId='primary', pageToken=page_token,
                                         timeMax=local_time.isoformat()).execute()
        for event in events['items']:
            summary = event.get('summary')
            date = event.get('start').get('dateTime')
            if date == None: date = ""
            if summary == None: summary = ""
            print(summary + " " + date)
        page_token = events.get('nextPageToken')
        if not page_token:
            break

    # _events = g.get_events()
    # print()

    # print( calendar['summary'])
