"""
In search_problem.py, you will implement generic search_problem algorithms
"""
from search_problem.src import util
from search_problem.src.util import PriorityQueueWithFunction


class SearchProblem:
    """
    This class outlines the structure of a search_problem problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def get_start_state(self):
        """
        Returns the start state for the search_problem problem
        """
        util.raiseNotDefined()

    def is_goal_state(self, state):
        """
        state: Search statex

        Returns True if and only if the state is a valid goal state
        """
        util.raiseNotDefined()

    def get_successors(self, state):
        """
        state: Search state

        For a given state, this should return a list of triples,
        (successor, action, stepCost), where 'successor' is a
        successor to the current state, 'action' is the action
        required to get there, and 'stepCost' is the incremental
        cost of expanding to that successor
        """
        util.raiseNotDefined()

    def get_cost_of_actions(self, actions):
        """
        actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.  The sequence must
        be composed of legal moves
        """
        util.raiseNotDefined()


# BEGIN SOLUTION
class Node:
    """
    AIMA: A node in a search_problem tree. Contains a pointer
    to the parent (the node that this is a successor of) 
    and to the actual state for this node. Note that if 
    a state is arrived at by two paths, then there are 
    two nodes with the same state.  Also includes the 
    action that got us to this state, and the total 
    path_cost (also known as g) to reach the node.  
    Other functions may add an f and h value; see 
    best_first_graph_search and a_star_search for an
    explanation of how the f and h values are handled. 
    You will not need to subclass this class.
    """

    def __init__(self, state, parent=None,path_cost=0):
        """
        Create a search_problem tree Node, derived from a parent by an action.
        """
        self.state = state
        self.parent = parent
        if parent:
            self.path_cost = parent.path_cost + path_cost
            self.depth = parent.depth + 1
        else:
            self.path_cost = path_cost
            self.depth = 0

    def __repr__(self):
        return "<Node %s>" % (self.state,)





    def expand(self, problem):
        """
        Return a list of nodes reachable from this node. [Fig. 3.8]
        """
        return [Node(_next, self, cost)
                for (_next, cost) in problem.get_successors(self.state)]


REVERSE_PUSH = False


def graph_search(problem, fringe):
    """
    Search through the successors of a problem to find a goal.
    The argument fringe should be an empty queue.
    """
    fringe.push(Node(problem.get_start_state()))

    visited = set()
    while not fringe.isEmpty():
        node = fringe.pop()
        if problem.is_goal_state(node.state):
            return node.state

        in_visited = node.state in visited

        if not in_visited:
            visited.add(node.state)
            next_nodes = node.expand(problem)
            if REVERSE_PUSH:
                next_nodes.reverse()
            for next_node in next_nodes:
                problem.expanded +=1
                fringe.push(next_node)
    return None

def graph_max_goal_search(problem, fringe: PriorityQueueWithFunction):
    """
    Search through the successors of a problem to find a goal.
    The argument fringe should be an empty queue.
    """
    max_node = problem.get_start_state()
    max_value = fringe.evaluate(max_node)
    fringe.push(Node(problem.get_start_state()))

    visited = set()
    while not fringe.isEmpty():
        node = fringe.pop()
        if problem.is_goal_state(node.state):
            if max_value > fringe.evaluate(node):
                max_value = fringe.evaluate(node)
                max_node = node

        in_visited = node.state in visited
        if not in_visited:
            visited.add(node.state)
            next_nodes = node.expand(problem)
            if REVERSE_PUSH:
                next_nodes.reverse()
            for next_node in next_nodes:
                fringe.push(next_node)
    return max_node, max_value


# END SOLUTION NO PROMPT


def depth_first_search(problem):
    """
    Search the deepest nodes in the search_problem tree first.

    Your search_problem algorithm needs to return a list of actions that reaches
    the goal. Make sure to implement a graph search_problem algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search_problem problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """
    "*** YOUR CODE HERE ***"
    # BEGIN SOLUTION METHOD
    print(problem.get_start_state())
    return graph_search(problem, util.Stack())
    # END SOLUTION
    util.raiseNotDefined()


def breadth_first_search(problem):
    """
    Search the shallowest nodes in the search_problem tree first.
    """
    "*** YOUR CODE HERE ***"
    # BEGIN SOLUTION METHOD
    return graph_search(problem, util.Queue())
    # END SOLUTION
    util.raiseNotDefined()


def uniform_cost_search(problem):
    """
    Search the node of least total cost first.
    """
    "*** YOUR CODE HERE ***"
    # BEGIN SOLUTION METHOD
    return graph_search(problem,
                        util.PriorityQueueWithFunction(
                            lambda node: node.path_cost))
    # END SOLUTION
    util.raiseNotDefined()


def null_heuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0


def a_star_search(problem, heuristic=null_heuristic):
    """
    Search the node that has the lowest combined cost and heuristic first.
    """
    "*** YOUR CODE HERE ***"
    # BEGIN SOLUTION METHOD
    return graph_search(problem,
                        util.PriorityQueueWithFunction(
                                lambda node: node.path_cost + heuristic(node.state, problem)))
    # END SOLUTION
    util.raiseNotDefined()

    # BEGIN SOLUTION


def greedy_search(problem, heuristic=null_heuristic):
    """
    Search the node that has the lowest heuristic first.
    """
    return graph_search(problem, util.PriorityQueueWithFunction(
        lambda node: heuristic(node.state, problem)))
    # END SOLUTION NO PROMPT


# Abbreviations
bfs = breadth_first_search
dfs = depth_first_search
astar = a_star_search
ucs = uniform_cost_search
