from search_problem.src.CalenderTipes import EventsList
from search_problem.src import util

REVERSE_PUSH = False
import search_problem.algorithms.utils as search
class Node:
    """
    AIMA: A node in a search_problem tree. Contains a pointer
    to the parent (the node that this is a successor of)
    and to the actual state for this node. Note that if
    a state is arrived at by two paths, then there are
    two nodes with the same state.  Also includes the
    action that got us to this state, and the total
    path_cost (also known as g) to reach the node.
    Other functions may add an f and h value; see
    best_first_graph_search and a_star_search for an
    explanation of how the f and h values are handled.
    You will not need to subclass this class.
    """

    def __init__(self, state, parent=None, action=None, path_cost=0):
        """
        Create a search_problem tree Node, derived from a parent by an action.
        """
        self.state = state
        self.parent = parent
        self.action = action
        if parent:
            self.path_cost = parent.path_cost + path_cost
            self.depth = parent.depth + 1
        else:
            self.path_cost = path_cost
            self.depth = 0

    def __repr__(self):
        return "<Node %s>" % (self.state,)


    def expand(self, problem):
        """
        Return a list of nodes reachable from this node. [Fig. 3.8]
        """
        return [Node(_next, self, cost)
                for (_next,  cost) in problem.get_successors(self.state)]


def graph_search(problem, fringe):
    """
    Search through the successors of a problem to find a goal.
    The argument fringe should be an empty queue.
    """
    fringe.push(Node(problem.get_start_state()))

    visited = set()
    while not fringe.isEmpty():
        node = fringe.pop()
        if problem.is_goal_state(node.state):
            return node.path()

        in_visited = node.state in visited

        if not in_visited:
            visited.add(node.state)
            next_nodes = node.expand(problem)
            if REVERSE_PUSH:
                next_nodes.reverse()
            for next_node in next_nodes:
                fringe.push(next_node)
    return None

def null_heuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def intersection_complemntry_heuristic(state: search.CalNode, problem=None):
    """
    A heuristic function estimates the cost to the goal state as the cost to the next node.
    """
    return search.day_min_value_hueristic(state.state, problem.extractor)

def intersection_complemntry_best_posible_values_hueristic(state: search.CalNode, problem: search.ScheduleSearchProblemAStar =None):
    """
    A heuristic function estimates the cost to the goal state as the cost to the next node.
    """

    some = 0
    for data in state.events_to_schedule:
        some+= problem.get_cost_of_actions(EventsList([problem._get_best_assingment_event(state.state, data)]))
    print(some)
    return some

# def gready_first_search(problem, fringe):


def a_star_search(problem, heuristic=null_heuristic):
    """
    Search the node that has the lowest combined cost and heuristic first.
    """
    # BEGIN SOLUTION METHOD
    return graph_search(problem,
                        util.PriorityQueueWithFunction(
                                lambda node: node.path_cost + heuristic(node.state, problem)))


