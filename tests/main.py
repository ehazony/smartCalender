
from search_problem.algorithms.utils import *
from search_problem.src.CalenderTipes import *
from search_problem.src.CalenderTipes import task
import sys
import time


def get_input():
    data = []
    while True:
        name = input("task name:")
        if name == "q":
            break
        duration = input("duration:")

        duration = datetime.timedelta(hours=float(duration))
        data.append(task(name, duration))
    return data


def parce_args(arg):
    if arg == "today":
        return ProblemName.TODAY
    if arg == "tomorrow":
        return ProblemName.TOMORROW
    if arg == "week":
        return ProblemName.THIS_WEEK
    if arg == "next_week":
        return ProblemName.NEXT_WEEK


def print_base_info(state: EventsList, start: datetime.datetime, end: datetime.datetime):
    print("start time: " + start.isoformat())
    print("end time: " + end.isoformat())
    print("*****")
    [print(event) for event in state.get_events()]
    print("*****")


def get_Astar_problem(problem_name: ProblemName, input_data, print_base=False):
    cal = GoogleCalendar()
    base_day, time = cal.get_events(problem_name)
    problem = ScheduleSearchProblemAStar(base_day, cal, time[0], time[1],input_data)
    print_base_info(base_day, time[0], time[1])

    return problem, base_day, cal, time



def insert_salution(salution, cal):
    for event in salution:
        if event.moveable:
            cal.insert_event(event)


def update(salution, cal):
    i = input("press T to update F to cancel:")
    if i == "T":
        insert_salution(salution, cal)


if __name__ == "__main__":
    problem_name = parce_args(sys.argv[1])
    input_data = get_input()


    A_star_problem, state, cal, t = get_Astar_problem(problem_name, input_data, True)
    start= time.time()
    salution = A_star_problem.get_best_assignment_trim(A_star_problem.base_day, input_data,0)
    end = time.time()
    print("time for finding salution: "+str(end- start))
    print("number expanded by trim search: " + str(A_star_problem.expanded))
    print("value of day_huristic for trim search: "+ str(day_value_heuristic(salution[1],  A_star_problem.extractor)))

    # A_star_problem, state, cal, t = get_Astar_problem(problem_name, input_data, True)
    # start= time.time()
    # salution = A_star_problem.get_Astar_assignment(search_helper.intersection_complemntry_best_posible_values_hueristic)
    # end = time.time()
    # print_base_info(salution, t[0], t[1])
    # print("time for finding salution: "+str(end- start))
    # print("number expanded by A* search: " + str(A_star_problem.expanded))
    # print("value of day_huristic for A*: "+ str(day_value_heuristic(salution,  A_star_problem.extractor)))
    update(salution, cal)
    # print("---------------------------------------------------------------")


