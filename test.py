import xlsxwriter

from search_problem.algorithms.AStarSearch import ScheduleSearchProblemAStar
from search_problem.algorithms.BFSSearch import ScheduleSearchProblemBFS
from search_problem.algorithms.TrimSearch import ScheduleSearchProblemTrim
from search_problem.algorithms.utils import *
import time
from search_problem.algorithms.GeneticSearch import GASearchProblem
from search_problem.algorithms.AnnealSearch import AnnealProblem
import signal

OUTPUT_FILE = "out_week_tasks5_annealcompare.xlsx"
cal = GoogleCalendar()
extractor = CalendarInfoExtractor(cal)
workbook = xlsxwriter.Workbook(OUTPUT_FILE, {'strings_to_numbers': True})
worksheet = workbook.add_worksheet()


# def timeout_handler(signum, frame):
#     raise Exception("end of time")
#
#
# signal.signal(signal.SIGALRM, handler=timeout_handler)


class test_info:
    def __init__(self, base_day, tasks):
        self.base_day = base_day
        self.tasks = tasks


def test(problems, tests_info):
    i = 0
    worksheet.write_row(0, 0, ["algorithm", "score", "nodes expanded", "time"])
    for test in tests_info:

        for problem in problems:
            i += 1
            try:
                data = []
                # signal.alarm(36000)
                print("solution name: {}".format(problem.get_name()))
                print("number of tasks in test: {}".format(len(test.tasks)))
                start = time.time()
                salution, score, expanded = problem.get_assignment(test.base_day, test.tasks)
                end = time.time()
                if salution is not None:
                    print("time for finding solution: {}".format(end - start))
                    print("number nods expanded: {}".format(expanded))
                    print("value of day_huristic: {}".format(day_value_heuristic(salution, problem.extractor)))
                    print(salution)
                    print("------------------------------------------------------")
                    data = [problem.get_name(), day_value_heuristic(salution, problem.extractor), expanded, end - start]
            except Exception as exc:
                raise exc
                print("{} cant find solution".format(problem.get_name()))
                print(exc)
                print("number nods expanded: {}".format(problem.expanded))
            worksheet.write_row(i, 0, data)


def load_problems(cal):
    # return [AnnealProblem(cal), GASearchProblem(cal), ScheduleSearchProblemAStar(cal), ScheduleSearchProblemTrim(cal),
    #         ScheduleSearchProblemBFS(cal)]
    a500 = AnnealProblem(cal)
    a500.name = "anneal 500"
    a1000 = AnnealProblem(cal)
    a1000.name = "anneal 1000"
    a1000.set_schudle(a1000.Tmax, a1000.Tmin, 1000, a1000.updates)
    a1500 = AnnealProblem(cal)
    a1500.name = "anneal 1500"
    a1500.set_schudle(a1000.Tmax, a1000.Tmin, 1500, a1000.updates)
    return [ a500, a1000, a1500]


# , GASearchProblem(cal), ScheduleSearchProblemAStar(cal), ScheduleSearchProblemTrim(cal), ScheduleSearchProblemBFS(cal)

def get_random_tasks(num):
    tasks = []
    occurrences_intervals = extractor.get_occurrences_intervals()
    commen_summerys = [summery for summery,intervlas in occurrences_intervals.items() if len(intervlas) > 3]
    for i in range(num):
        duration = datetime.timedelta(hours=random.randint(1, 3))
        summery = random.choice(commen_summerys)
        tasks.append(task(summery, duration))
    return tasks


def get_day_test(num_events):
    tasks = get_random_tasks(num_events)
    start = datetime.datetime.now().replace(hour=6, minute=0, second=0)
    start = start - datetime.timedelta(days=random.randint(1, 100))
    end = start + datetime.timedelta(hours=15)
    day = cal.get_events_in_interval(start, end)
    return test_info(day, tasks)


def get_week_test(num_events):
    tasks = get_random_tasks(num_events)
    start = datetime.datetime.now().replace(hour=6, minute=0, second=0)
    start = start - datetime.timedelta(days=random.randint(1, 100))
    end = start + datetime.timedelta(days=7)
    day = cal.get_events_in_interval(start, end)
    return test_info(day, tasks)


if __name__ == '__main__':
    tests_info = []
    problems = load_problems(cal)
    # start = datetime.datetime.now().replace(hour=6, minute=0, second=0)
    # end = start + datetime.timedelta(hours=12)
    # base_day, (start, end) = cal.get_today_events()
    # tasks = [task("לסדר", datetime.timedelta(hours=1)), task("א.צ", datetime.timedelta(hours=1)),
    #          task("חדר כושר", datetime.timedelta(hours=2)),
    #          ]
    # tests_info.append(test_info(base_day, start, end, tasks))
    for i in range(10):
        tests_info.append(get_week_test(5))
    test(problems, tests_info)
    workbook.close()
